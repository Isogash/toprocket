var users = [
    { ID:0, user_login: "u1@fp.com", display_name:"User1" },
    { ID:1, user_login: "u1@fp.com", display_name:"User1" },
    { ID:2, user_login: "u2@fp.com", display_name:"User Two" },
    { ID:3, user_login: "User3", display_name:"User three" },
    { ID:4, user_login: "User4_four", display_name:"UserFour" },
    { ID:5, user_login: "u5", display_name:"User_five" },
    { ID:6, user_login: "u6@fp.com", display_name:"u6" }];


// Add cookie to your browser to mock - run this on the page and refresh:
// document.cookie="WP_MOCK_LOGGED_IN=1;";

function WP_Mock() {
    this.cookiename = "WP_MOCK_LOGGED_IN";
}
WP_Mock.prototype.checkAuth = function (req, cb) {
    var self = this;
    var logged_in = false;
    var id = 1;
    if ( req.headers.cookie ) {
		req.headers.cookie.split( ';' ).forEach( function( cookie ) {
			if ( cookie.split( '=' )[0].trim() == self.cookiename ) {
				logged_in = true;
				id = parseInt(cookie.split('=')[1].trim());
			}
		} );
        return {
            on: function(e, cb) {
                var user = users[id];
                user.id = user.ID;
                cb(true, user);
            }
        }
    }
	else {
		return {
            on: function(e, cb) {
                cb(false, 0);
            }
		}
	}
}
WP_Mock.prototype.getUsers = function (cb) {
    cb(users);
}
WP_Mock.prototype.getUser = function (id, cb) {
    cb(users[id]);
}

module.exports.create = function () {
  return new WP_Mock();
};