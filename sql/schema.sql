CREATE DATABASE `TopRocket`;

USE `TopRocket`;

CREATE TABLE `Pack` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`description` varchar(300) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `PackStat` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`pack_id` bigint NOT NULL,
	`name` varchar(30) NOT NULL,
	`format` varchar(15) NULL,
	`bigger_wins` boolean NOT NULL,
	PRIMARY KEY (`pack_id`, `id`)
) ENGINE=MyISAM;

CREATE TABLE `Card` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`pack_id` bigint NOT NULL,
	`name` varchar(30) NOT NULL,
	`title` varchar(50) NOT NULL,
	`subtitle` varchar(50) NOT NULL,
	`flavour` varchar(50) NULL,
	`team_name` varchar(50) NULL,
	`tag` varchar(50) NULL,
	`nationality` varchar(50) NULL,
	PRIMARY KEY (`pack_id`, `id`)
) ENGINE=MyISAM;

CREATE TABLE `CardStat` (
	`pack_id` bigint NOT NULL,
	`card_id` bigint NOT NULL,
	`id` bigint NOT NULL,
	`value` DECIMAL(16, 2) NOT NULL,
	PRIMARY KEY (`pack_id`, `card_id`, `id`)
) ENGINE=MyISAM;

CREATE TABLE `Player` (
	`id` bigint NOT NULL,
	`wins` bigint NOT NULL,
	`draws` bigint NOT NULL,
	`losses` bigint NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `Game` (
	`player1_id` bigint NOT NULL,
	`player2_id` bigint NOT NULL,
	`time` bigint NOT NULL,
	`outcome` int NOT NULL,
	`challenger_id` bigint NULL,
	PRIMARY KEY (`player1_id`,`player2_id`,`time`)
) ENGINE=InnoDB;

-- Can't use FKs with MyISAM
-- ALTER TABLE `PackStat` ADD CONSTRAINT `PackStat_fk0` FOREIGN KEY (`pack_id`) REFERENCES `Pack`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `Card` ADD CONSTRAINT `Card_fk0` FOREIGN KEY (`pack_id`) REFERENCES `Pack`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `CardStat` ADD CONSTRAINT `CardStat_fk0` FOREIGN KEY (`pack_id`, `card_id`) REFERENCES `Card`(`pack_id`, `id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `CardStat` ADD CONSTRAINT `CardStat_fk1` FOREIGN KEY (`pack_id`, `id`) REFERENCES `PackStat`(`pack_id`, `id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `Game` ADD CONSTRAINT `Game_fk0` FOREIGN KEY (`player1_id`) REFERENCES `Player`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `Game` ADD CONSTRAINT `Game_fk1` FOREIGN KEY (`player2_id`) REFERENCES `Player`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

