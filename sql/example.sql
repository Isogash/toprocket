-- Run with:
-- mysql-ctl cli
-- source sql/example.sql
-- exit

USE TopRocket;


-- NB: This assumes it is the first pack, i.e. that both the pack and the card tables are empty and their identities at 1

--
-- Call once for the pack. Name and then description. The id will be 1.
CALL InsertPack('Pack1 name', 'Pack 1 description');
-- Call once for each attribute that will be on each card. 0, name, format, bigger_wins (0=false, 1=true)
CALL InsertPackStat(1, 'Strength', '', 1);
CALL InsertPackStat(1, 'Height', '', 1);
CALL InsertPackStat(1, 'Weight', '', 1);
CALL InsertPackStat(1, 'Endurance', '', 1);
CALL InsertPackStat(1, 'Bravery', '', 1);

-- Call once for each card name, title, subtitle, flavour. card id will be 1 incrementing
CALL InsertCard(1, 'Pseidon', 'Pseidon Strong', 'Pseidon works out', 'Wet', 'team1', 't1', 'USA');
-- Call with the value for each attribute for the card. 0, card_id, stat_id, value
CALL InsertCardStat(1, 1, 1, 0);
CALL InsertCardStat(1, 1, 2, 201);
CALL InsertCardStat(1, 1, 3, 202);
CALL InsertCardStat(1, 1, 4, 500);
CALL InsertCardStat(1, 1, 5, 204);

CALL InsertCard(1, 'Hades', 'Hades Tall', 'Hades took HGH', 'Fire', 'team1', 't1', 'USA');
CALL InsertCardStat(1, 2, 1, 300);
CALL InsertCardStat(1, 2, 2, 601);
CALL InsertCardStat(1, 2, 3, 302);
CALL InsertCardStat(1, 2, 4, 103);
CALL InsertCardStat(1, 2, 5, 304);

CALL InsertCard(1, 'Hermes', 'Hades Fast', 'Hermes is on speed', 'Friction burn', 'team2', 't2.', 'Russia');
CALL InsertCardStat(1, 3, 1, 400);
CALL InsertCardStat(1, 3, 2, 101);
CALL InsertCardStat(1, 3, 3, 402);
CALL InsertCardStat(1, 3, 4, 203);
CALL InsertCardStat(1, 3, 5, 404);

CALL InsertCard(1, 'Zeus', 'Zeus Fatty', 'Zeus put on some weight', 'Strawberry', 'team2', 't2.', 'Belarus');
CALL InsertCardStat(1, 4, 1, 100);
CALL InsertCardStat(1, 4, 2, 801);
CALL InsertCardStat(1, 4, 3, 302);
CALL InsertCardStat(1, 4, 4, 203);
CALL InsertCardStat(1, 4, 5, 104);


--  Requires root (only debug values anyway)
INSERT INTO Player VALUES
    (1, 143000, 0, 0),
    (606, 12, 0, 2),
    (19, 2, 0, 15),
    (22, 5, 0, 1),
    (2, 123, 0, 7),
    (3, 34, 0, 57),
    (4, 0, 0, 1);