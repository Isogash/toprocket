-- trumpsuser (run as this user most of the time)
GRANT USAGE ON TopRocket.* TO 'trumpsuser'@'localhost' IDENTIFIED BY '3ni1.293F2£$0i@S';
-- GRANT SELECT ON `TopRocket`.* TO 'trumpsuser'@'localhost';
-- GRANT INSERT ON `TopRocket`.`Game` TO 'trumpsuser'@'localhost';
-- GRANT UPDATE (`wins`, `draws`, `losses`) ON `TopRocket`.`Player` TO 'trumpsuser'@'localhost';

-- trumpsadmin (only run as this user for administrative tasks)
GRANT USAGE ON TopRocket.* TO 'trumpsadmin'@'localhost' IDENTIFIED BY '£B"i3fb2$%asbid3';
-- GRANT DELETE, INSERT, SELECT, UPDATE ON `TopRocket`.* TO 'trumpsadmin'@'localhost';