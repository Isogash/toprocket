DELIMITER $$
CREATE PROCEDURE `GetPlayer` (player_id BIGINT)
BEGIN

    SELECT `id`, `wins`, `draws`, `losses`, `position` 
    FROM(SELECT *, @rownum:=@rownum+1 AS `position` FROM `Player`, (SELECT @rownum:=0) AS r ORDER BY `wins` DESC) AS d
        WHERE `id` = player_id;

END$$
GRANT EXECUTE ON PROCEDURE `GetPlayer` TO 'trumpsuser'@'localhost'$$
GRANT EXECUTE ON PROCEDURE `GetPlayer` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `SaveGameResult` (p1_id BIGINT,
                                   p2_id BIGINT,
                                   js_time BIGINT,
                                   result INT,
                                   challenger BIGINT)
BEGIN
    
    DECLARE winner_id, loser_id BIGINT;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
    BEGIN
        ROLLBACK;
    END;
    
    -- First ID in the Game table should be the winner, if there was one
    SET winner_id := CASE result WHEN -1 THEN p2_id ELSE p1_id END;
    SET loser_id := CASE result WHEN -1 THEN p1_id ELSE p2_id END;
    
    START TRANSACTION;
    
        -- Insert into Game table
        INSERT INTO `Game` (`player1_id`, `player2_id`, `time`, `outcome`, `challenger_id`)
            VALUES (winner_id, loser_id, js_time, result, challenger);
        
        -- Update Player table
        IF result = 0 THEN
            UPDATE `Player`
                SET `draws` = `draws` + 1
                WHERE `id` = p1_id OR `id` = p2_id;
        ELSE
            UPDATE `Player`
                SET `wins` = `wins` + 1
                WHERE `id` = winner_id;
            UPDATE `Player`
                SET `losses` = `losses` + 1
                WHERE `id` = loser_id;
        END IF;
        
    COMMIT;

END$$
GRANT EXECUTE ON PROCEDURE `SaveGameResult` TO 'trumpsuser'@'localhost'$$

CREATE PROCEDURE `GetPacks` ()
BEGIN

    SELECT `id`, `name`, `description` FROM `Pack`;

END$$
GRANT EXECUTE ON PROCEDURE `GetPacks` TO 'trumpsuser'@'localhost'$$
GRANT EXECUTE ON PROCEDURE `GetPacks` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `GetPackStats` (pack BIGINT)
BEGIN

    SELECT `id`, `name`, `format`, `bigger_wins` FROM `PackStat`
        WHERE  `pack_id` = pack;

END$$
GRANT EXECUTE ON PROCEDURE `GetPackStats` TO 'trumpsuser'@'localhost'$$
GRANT EXECUTE ON PROCEDURE `GetPackStats` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `GetCards` (pack BIGINT)
BEGIN

    SELECT `id`, `pack_id`, `name`, `title`, `subtitle`, `flavour`, `team_name`, `tag`, `nationality` FROM `Card`
        WHERE `pack_id` = pack;

END$$
GRANT EXECUTE ON PROCEDURE `GetCards` TO 'trumpsuser'@'localhost'$$
GRANT EXECUTE ON PROCEDURE `GetCards` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `GetCardStats` (pack BIGINT, card BIGINT)
BEGIN

    SELECT `id`, `value` FROM `CardStat`
        WHERE `pack_id` = pack AND `card_id` = card;

END$$
GRANT EXECUTE ON PROCEDURE `GetCardStats` TO 'trumpsuser'@'localhost'$$
GRANT EXECUTE ON PROCEDURE `GetCardStats` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `InsertPlayer` (player_id BIGINT)
BEGIN

    INSERT INTO `Player` (`id`, `wins`, `draws`, `losses`)
    VALUES (player_id, 0, 0, 0);

END$$
GRANT EXECUTE ON PROCEDURE `InsertPlayer` TO 'trumpsuser'@'localhost'$$
GRANT EXECUTE ON PROCEDURE `InsertPlayer` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `InsertPack` (pack_name VARCHAR(50), pack_description VARCHAR(300))
BEGIN

    INSERT INTO `Pack` (`name`, `description`)
    VALUES (pack_name, pack_description);

END$$
GRANT EXECUTE ON PROCEDURE `InsertPack` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `InsertPackStat` (pack_id BIGINT,
                                   pack_name VARCHAR(30),
                                   pack_format VARCHAR(15),
                                   pack_bigger_wins BOOLEAN)
BEGIN

    INSERT INTO `PackStat` (`pack_id`, `name`, `format`, `bigger_wins`)
    VALUES (pack_id, pack_name, pack_format, pack_bigger_wins);

END$$
GRANT EXECUTE ON PROCEDURE `InsertPackStat` TO 'trumpsadmin'@'localhost'$$


CREATE PROCEDURE `InsertCard` (pack_id BIGINT,
                               card_name varchar(30),
                               card_title varchar(50),
                               card_subtitle varchar(50),
                               card_flavour varchar(50),
                               card_team_name varchar(50),
                               card_tag varchar(50),
                               card_nationality varchar(50))
BEGIN

    INSERT INTO `Card` (`pack_id`, `name`, `title`, `subtitle`, `flavour`, `team_name`, `tag`, `nationality`)
    VALUES (pack_id, card_name, card_title, card_subtitle, card_flavour, card_team_name, card_tag, card_nationality);

END$$
GRANT EXECUTE ON PROCEDURE `InsertCard` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `InsertCardStat` (pack_id BIGINT,
                                   card_id BIGINT,
                                   cardstat_id BIGINT,
                                   cardstat_value  DECIMAL(16, 2))
BEGIN

    INSERT INTO `CardStat` (`pack_id`, `card_id`, `id`, `value`)
    VALUES (pack_id, card_id, cardstat_id, cardstat_value);

END$$
GRANT EXECUTE ON PROCEDURE `InsertCardStat` TO 'trumpsadmin'@'localhost'$$

CREATE PROCEDURE `GetLeaderboard` ()
BEGIN

    SELECT `id`, `wins`, `draws`, `losses`, @rownum:=@rownum+1 AS `position` FROM `Player`, (SELECT @rownum:=0) AS r
        ORDER BY `wins` DESC
        LIMIT 25;

END$$
GRANT EXECUTE ON PROCEDURE `GetLeaderboard` TO 'trumpsuser'@'localhost'$$
GRANT EXECUTE ON PROCEDURE `GetLeaderboard` TO 'trumpsadmin'@'localhost'$$

DELIMITER ;