var util = require('util');

var config = {};
module.exports = config;

config.database = {};
config.database.db_host = "localhost";
config.database.db_user = "trumpsuser";
config.database.db_password = "3ni1.293F2£$0i@S";
config.database.db_admin_user = "trumpsadmin";
config.database.db_admin_password = '£B"i3fb2$%asbid3';
config.database.db = "TopRocket";

config.port = 8081;
config.ip = "";
/* NB: Make sure you also change the url in index.html for socket.io */
config.homeUrl = "http://www.fpspulse.com/trumps";

config.numberOfCardsInGame = 30;
config.challengeGamesAreValid = true;

config.wp_auth = {};
config.wp_auth.required = true;
config.wp_auth.mock = true;
config.wp_auth.url = "";
config.wp_auth.LOGGED_IN_KEY = "";
config.wp_auth.LOGGED_IN_SALT = "";
config.wp_auth.mysql_host = "localhost";
config.wp_auth.mysql_username = "";
config.wp_auth.mysql_password = "";
config.wp_auth.mysql_database = "";
config.wp_auth.wp_prefix = "wp_";
config.wp_auth.loginUrl = util.format("http://www.fpspulse.com/wp-login.php?redirect_to=%s&action=bpnoaccess", encodeURIComponent(config.homeUrl));