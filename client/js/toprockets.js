var app = angular.module('topRocketsApp', []);

var playTurn, challenge;

app.controller('TopRocketsController', ['$scope', function($scope) {
    var socket = io.connect(config.socketio.url, config.socketio.options);
    var username = '';
    //var mycard = {name:'', stats:[]};
    
    $scope.timeleft = 0;
    $scope.timerInterval = {};
    $scope.pack = {};
    $scope.state = '';
    $scope.newstate = '';
    $scope.name = '';
    $scope.theirname = '';
    $scope.wongame = false;
    $scope.gameover = false;
    $scope.winner = '';
    $scope.myturn = false;
    $scope.mycardnumber = 0;
    $scope.theircardnumber = 0;
    $scope.stat = 0;
    
    socket.on('connect', function () {
      console.log("connected");
      $scope.changeState('login');
    });
    
    socket.on('login', function(player) {
      username = player.username;
      $scope.name = username;
      $scope.display_name = player.display_name;
      $scope.changeState('lobby');
    });
    
    socket.on('newgame', function(data) {
      $scope.updatePlayerInfo(data.playerdata);
      
      $scope.pack = data.packdata;
    });
    
    socket.on('newturn', function(data) {
      $scope.myturn = data.playerturn == username;
      //mycard = data.yourcard;
      
      $scope.changeTurn($scope.myturn);
      
      $scope.timeleft = 15;
      $scope.updateTimer();
      window.clearInterval($scope.timerInterval);
      $scope.timerInterval = window.setInterval(function() {
        if($scope.timeleft > 0) {
          $scope.timeleft--;
          $scope.updateTimer();
        } else {
          window.clearInterval($scope.timerInterval);
        }
      }, 1000);

      $scope.mycard = data.yourcard;
      $scope.changeState('turn-start');
      $scope.updateCard($scope.mycard, 'my', $scope.myturn);
    });
    
    socket.on('endturn', function(data) {
      window.clearInterval($scope.timerInterval);
      $scope.updatePlayerInfo(data.playerdata);
      $scope.winner = (data.winner == username);
      $scope.theircard = data.cardsplayed[$scope.theirname];
      $scope.stat = data.stat;
      $scope.changeState('turn-end');
      if(!$scope.winner) {
        $('#nextturn').hide();
      }
      $scope.updateCard($scope.mycard, 'my', false, $scope.winner, $scope.stat);
      $scope.updateCard($scope.theircard, 'their', false, !$scope.winner, $scope.stat);
    });
    
    socket.on('disconnect', function() {
      $scope.changeState('loading');
      socket = io.connect();
    });
    
    socket.on('playerleft', function(player) {
      //display something 
    });
    
    socket.on('gameover', function(won) {
      $scope.wongame = won;
      $scope.gameover = true;
      if($scope.wongame) {
        $scope.changeState('gameover-win');
        $scope.gameover = false;
      } else {
        $scope.changeState('gameover-lose');
        $scope.gameover = false;
      }
    });
    
    socket.on('playerlist', function(data) {
      $scope.updatePlayerList(data);
    });
    
    socket.on('playerlistadd', function(data) {
      $scope.addPlayer(data);
    });
    
    socket.on('playerlistremove', function(data) {
      $scope.removePlayer(data);
    });
    
    socket.on('joinedqueue', function() {
      $('tr-btn-challenge').prop('disabled', true);
      $scope.changeState('searching');
    });
    
    socket.on('leftqueue', function() {
      $scope.changeState('lobby');
      $('tr-btn-challenge').prop('disabled', false);
    });
    
    socket.on('challengesent', function(target) {
      $scope.changeState('challenging');
      $('tr-btn-challenge').prop('disabled', true);
      $('#challengingtext').text('Waiting for ' + target);
    });
    
    socket.on('challengedeclined', function(target) {
      if(!($scope.state === 'challenging')) {
        $scope.changeState('lobby');
        $('tr-btn-challenge').prop('disabled', false);
        $('#lobbytext').text(target + ' declined your challenge.');
      }
    });
    
    socket.on('challengeblocked', function(target) {
      $scope.changeState('lobby');
      $('tr-btn-challenge').prop('disabled', false);
      $('#lobbytext').text('Could not challenge ' + target + ' at this time.');
    });
    
    socket.on('challengerecieved', function(challenger) {
      $scope.changeState('challenge');
      $('#challengername').text(challenger);
    });
    
    socket.on('challengecancelled', function() {
      $scope.changeState('lobby');
      $('#lobbytext').text('The challenge was cancelled.');
      $('tr-btn-challenge').prop('disabled', false);
    });
    
    socket.on('leaderboard', function(leaderboard) {
      $scope.fillLeaderboard(leaderboard);
    })
    
    socket.on('error', function (msg) {
      console.error('Socket error: ' + msg);
    })
    
    $scope.updatePlayerInfo = function updatePlayerInfo(playerdata) {
      for(var player in playerdata) {
        if(username != playerdata[player].username) {
          $scope.theirname = playerdata[player].username;
          $scope.their_display_name = playerdata[player].display_name;
          $scope.theircardnumber = playerdata[player].cards;
        } else {
          $scope.mycardnumber = playerdata[player].cards;
        }
      }
      $('#myname').text($scope.display_name);
      $('#mycards').text('CARDS LEFT: ' + $scope.mycardnumber);
      $('#theirname').text($scope.their_display_name);
      $('#theircards').text('CARDS LEFT: ' + $scope.theircardnumber);
    }
    
    $scope.finishTurn = function finishTurn() {
      socket.emit('endturn');
    }
    
    $scope.finishGameOver = function finishGameOver() {
      $scope.changeState('lobby');
      $('tr-btn-challenge').prop('disabled', false);
    }

    $scope.playTurn = function playTurn(stat) {
      if($scope.myturn && $scope.state == 'turn-start') {
        socket.emit('playturn', stat);
      }
    };
    
    $scope.leave = function leave() {
      socket.emit('leave');
    }
    
    $scope.startSearch = function startSearch() {
      socket.emit('startqueue');
    }
    
    $scope.cancelSearch = function cancelSearch() {
      socket.emit('cancelqueue');
    }
    
    $scope.challenge = function challenge(username) {
      console.log('Challenging ' + username);
      socket.emit('challenge', username);
    }
    
    $scope.challengeAccept = function challengeAccept() {
      socket.emit('challengeaccept');
    }
    
    $scope.challengeDecline = function challengeDecline() {
      socket.emit('challengedecline');
      $scope.changeState('lobby');
      $('#lobbytext').text('Declined the challenge.');
      $('tr-btn-challenge').prop('disabled', false);
    }
    
    $scope.cancelChallenging = function cancelChallenging() {
      socket.emit('cancelchallenge');
      $scope.changeState('lobby');
      $('#lobbytext').text('Cancelled the challenge.');
      $('tr-btn-challenge').prop('disabled', false);
    }
    
    $scope.openLeaderboard = function () {
      socket.emit("leaderboard");
      $scope.changeState("leaderboard");
    }
    
    $scope.closeLeaderboard = function() {
      $scope.changeState("lobby");
    }
    
    $scope.changeState = function changeState(newstate) {
      $('.state').not('.state-' + newstate).hide();
      $('.state.state-' + newstate).show();
      $('.state-enabled').not('state-' + newstate).prop('disabled', true);
      $('.state-enabled.state-' + newstate).prop('disabled', false);
      $scope.state = newstate;
      if($scope.state == 'lobby') {
        $('#lobbytext').text('');
        socket.emit('getplayerlist');
      }
    }
    
    $scope.changeTurn = function changeTurn(mine) {
      var newstate;
      if(mine) {
        newstate = 'mine';
      } else {
        newstate = 'theirs';
      }
      $('.turn').not('.turn-' + newstate).hide();
      $('.turn.turn-' + newstate).show();
      $('.turn-enabled').not('turn-' + newstate).prop('disabled', true);
      $('.turn-enabled.turn-' + newstate).prop('disabled', false);
      $scope.turn = newstate;
    }
    
    $scope.updatePlayerList = function updatePlayerList(playerlist) {
      $('#playerlist').empty();
      for(var player in playerlist)
      {
        $scope.addPlayer(playerlist[player]);
      }
    }
    
    $scope.addPlayer = function addPlayer(player) {
      //$scope.removePlayer(player.username);
      var status = player.ingame ? 'ingame' : 'online'
      var append1 = '<tr playername="' + player.player.username + '">';
      var append2 = '<td class="tr-td-playerlist-left">' +
        '<div class="tr-text-player">' +
        player.player.display_name +
        '</div>' +
        '<div>' +
        '<span class="tr-text-status tr-text-status-' + status + '">' +
        status +
        '</span>' +
        '<span class="tr-text-wins">' +
        player.wins +
        ' wins</span>' +
        '</div>' +
        '</td><td class="tr-td-playerlist-right">';
      if(player.player.username == username) {
        append2 += '<button class="tr-btn-challenge-disabled"></button>';  
      } else {
        append2 += '<button class="tr-btn-challenge" onclick="challenge(\'' + player.player.username + '\')"></button>';
      }
      append2 += '</td>'
      var append3 = '</tr>';   
      
      var playeritem = $("[playername='" + player.player.username + "']");
      if(playeritem.length) {
        playeritem.empty();
        playeritem.append(append2);
      } else {
        $('#playerlist').append(append1 + append2 + append3);
      }
    }
    
    $scope.removePlayer = function removePlayer(playername) {
      $("[playername='" + playername + "']").remove();
    }
    
    $scope.updateCard = function updateCard(card, id, hover, won, stat) {
      $('#' + id + '-stats').empty();
      for(var i = 0; i < card.stats.length; i++)
      {
        var cssclass = 'tr-text-stats';
        if(stat == i) {
          if(won) {
            cssclass = 'tr-text-stats-green';
          } else {
            cssclass = 'tr-text-stats-red';
          }
        } else if(hover) {
          cssclass = 'tr-text-stats-hover';
        }
        $('#' + id + '-stats').append(
          '<tr class="' + cssclass + '" id="' + id + '-stat-' + i + '" onclick="playTurn(' + i + ')"><td class="tr-stats-td-left">' +
          '<div style="position:absolute;left:0px;">' + $scope.pack.stats[i].name + '</div>' +
          '</td><td class="tr-stats-td-right">' +
          '<div style="position:absolute;right:0px;">' + numeral(card.stats[i].value).format($scope.pack.stats[i].format) + '</div>' +
          '</td></tr>'
        );
      }
      $('#' + id + '-title').text(card.title);
      $('#' + id + '-subtitle').empty().append('Real Name:<br>' + card.subtitle);
      $('#' + id + '-flavour').empty().append('Team: ' + card.team_name + '<br>Tag: ' + card.tag + '<br>Nationality: ' + card.nationality);
      var $card_img = $('#' + id + '-cardphoto');
      $card_img.prop('src', './img/' + $scope.pack.id + '/' + card.id + '.png').on('error', function() {
        $card_img.prop('src', './img/' + $scope.pack.id + '/nophoto.png');
      });
    }
    
    $scope.updateTimer = function () {
      $('.tr-timer').text($scope.timeleft);
    }
    
    $scope.fillLeaderboard = function (leaderboard) {
      var $leaderboard = $("#leaderboardlist-col1");
      $leaderboard.empty();
      for (var i = 0; i < leaderboard.length; i++) {
        if (i == 15) {
          $leaderboard = $("#leaderboardlist-col2");
          $leaderboard.empty();
        }
        var append = '<li class="tr-leaderboard-item">' +
          '<div class="tr-text-player">' + leaderboard[i].display_name + '</div>' + 
          '<div class="tr-text-score">' + leaderboard[i].wins + '</div></li>';
        $leaderboard.append(append);
      }
    }
    
    playTurn = $scope.playTurn;
    challenge = $scope.challenge;
    
    $scope.changeState('loading');
    
    // Remove stylesheet that is hiding all states now
    $("#style-hide-state").remove();
}]);

var numberOfDots = 1;

function changeDots() {
  numberOfDots++;
  if(numberOfDots > 4) numberOfDots = 1;
  var dots = '';
  for(var i = 0; i < numberOfDots; i++)
  {
    dots = dots + '.';
  }
  $(".tr-dots").text(dots);
}

window.setInterval('changeDots()', 500);