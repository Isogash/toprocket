module.exports = PackStat;

function PackStat(id, name, format, bigger_wins) {
    this.id = id;
    this.name = name;
    this.format = format;
    this.bigger_wins = bigger_wins;
}