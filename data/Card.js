module.exports = Card;

function Card(id, pack_id, name, title, subtitle, flavour, team_name, tag, nationality) {
    this.id = id;
    this.pack_id = pack_id;
    this.name = name;
    this.title = title;
    this.subtitle = subtitle;
    this.flavour = flavour;
    this.team_name = team_name;
    this.tag = tag;
    this.nationality = nationality;
    this.stats = [];
}