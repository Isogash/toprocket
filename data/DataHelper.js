var logger = require('../logger');
var util = require('util');
var async = require("async");
var _ = require('lodash');

var mysql = require('mysql');

var Player = require("./Player");
var Pack = require("./Pack");
var PackStat = require("./PackStat");
var Card = require("./Card");
var CardStat = require("./CardStat");

module.exports = DataHelper;

function DataHelper(config) {
    
    // NB: default max connections for mysql is 151, I believe.
    this._pool = mysql.createPool({
        connectionLimit : 50,
        host : config.database.host,
        user : config.database.db_user,
        password : config.database.db_password,
        database : config.database.db,
        supportBigNumbers: true
    });
    this._adminPool = mysql.createPool({
        connectionLimit : 5,
        host : config.database.host,
        user : config.database.db_admin_user,
        password : config.database.db_admin_password,
        database : config.database.db,
        supportBigNumbers: true
    });
    
    this._config = config;
    
    this._disposed = false;
}

DataHelper.prototype.dispose = function() {
    this._disposed = true;
    this._pool.end();
    this._adminPool.end();
}
DataHelper.prototype.getPlayer = function(id, cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL GetPlayer(?);", [id]);
    this._pool.query(sql, function (err, results) {
        if (err) return cb(err, undefined);
        logger.debug("GetPlayer(%d)=", id, results);
        
        var rows = results[0];
        if (rows.length == 0) return cb(new NotFoundError(), undefined);
        var player = new Player(id, rows[0]["wins"], rows[0]["draws"], rows[0]["losses"], rows[0]["position"]);
        cb(null, player);
    });
}
DataHelper.prototype.addPlayer = function (id, cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL InsertPlayer(?);", [id]);
    this._pool.query(sql, function (err, results) {
        if (err) return cb(err, undefined);
        logger.debug("InsertPlayer(%d)", id);
        
        var player = new Player(id, 0, 0, 0);
        cb(null, player);
    });
}
DataHelper.prototype.saveGameResult = function(p1, p2, time, result, challenger, cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL SaveGameResult(?, ?, ?, ?, ?);", [p1, p2, time, result, challenger]);
    this._pool.query(sql, function (err, results) {
        if (err) return cb(err, undefined);
        logger.debug("saveGameResult(%d, %d, %d, %d, %s)", p1, p2, time, challenger, result);
        
        cb(null, true);
    });
}
DataHelper.prototype.getPacks = function(cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL GetPacks();", []);
    this._pool.query(sql, function (err, results) {
        if (err) return cb(err, undefined);
        logger.debug("GetPacks()");
        
        var rows = results[0];
        var packs = [];
        var gets = [];
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var pack = new Pack(row["id"], row["name"], row["description"]);
            packs.push(pack);
            gets.push((function(pack_obj) {
                return function(cb) {
                    self._getPackStats(pack, cb);
                };
            })(pack));
        }
        async.parallel(gets, function(err, result) {
            if (err) return cb(err, undefined);
            
            cb(null, packs); 
        });
    });
}
DataHelper.prototype._getPackStats = function (pack, cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL GetPackStats(?);", [pack.id]);
    this._pool.query(sql, function(err, results) {
        if (err) return cb(err, undefined);
        logger.debug("GetPackStats(%d)=". id, results);
       
        var rows = results[0];
        for (var i = 0; i < rows.length; i++) {
           var row = rows[i];
           var stat = new PackStat(row["id"], row["name"], row["format"], row["bigger_wins"]);
           pack.stats.push(stat);
        }
       
        cb(null, pack);
    });
}
DataHelper.prototype.getCards = function(pack_id, cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL GetCards(?);", [pack_id]);
    this._pool.query(sql, function (err, results) {
        if (err) return cb(err, undefined);
        logger.debug("GetCards(%d)=", pack_id, results);
        
        var rows = results[0];
        var cards = [];
        var gets = [];
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var card = new Card(row["id"], row["pack_id"], row["name"], row["title"], row["subtitle"], row["flavour"], row["team_name"], row["tag"], row["nationality"]);
            cards.push(card);
            gets.push((function(card_obj) {
                return function(cb) {
                    self._getCardStats(card_obj, cb);
                };
            })(card));
        }
        async.parallel(gets, function(err, result) {
            if (err) return cb(err, undefined);
            
            cb(null, cards); 
        });
    });
}
DataHelper.prototype._getCardStats = function (card, cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL GetCardStats(?, ?);", [card.pack_id, card.id]);
    this._pool.query(sql, function(err, results) {
        if (err) return cb(err, undefined);
        logger.debug("GetCardStats(%d,%d)=", card.pack_id, card.id, results);
       
        var rows = results[0];
        for (var i = 0; i < rows.length; i++) {
           var row = rows[i];
           var stat = new CardStat(row["id"], row["value"]);
           card.stats.push(stat);
        }
       
        cb(null, card);
    });
}
DataHelper.prototype.getLeaderboard = function(cb) {
    if (this._disposed) {
        return cb(new DisposedError(), undefined);
    }
    var self = this;
    
    var sql = mysql.format("CALL GetLeaderboard();", []);
    this._pool.query(sql, function(err, results) {
        if (err) return cb(err, undefined);
        logger.debug("GetLeaderboard()=", results);
       
        var rows = results[0];
        var leaderboard = [];
        for (var i = 0; i < rows.length; i++) {
           var row = rows[i];
            var player = new Player(row["id"], row["wins"], row["draws"], row["losses"], row["position"]);
           leaderboard.push(player);
        }
        
        // Get usernames
        
       
        cb(null, leaderboard);
    });
};
DataHelper.prototype.addUserInfo = function (players, users) {
   if (!Array.isArray(players)) players = [players];
   for (var i = 0; i < players.length; i++) {
        var player = players[i];
        //player.userInfo = users[player.id];
        //player.username = player.userInfo.user_login;
        if (player.id in users) {
            player.display_name = users[player.id].display_name;
        } else {
            logger.error("User not found for player id %d", player.id);
            player.display_name = "";
        }
   } 
};

// DisposedError
util.inherits(DisposedError, Error);
function DisposedError() {
    Error.captureStackTrace(this, arguments.callee);
    this.message = "The data helper cannot be used after it has been disposed.";
}
DisposedError.prototype.toString = function() {
	return "DisposedError: " + util.inspect(this.message);
};

// NotFoundError
util.inherits(NotFoundError, Error);
function NotFoundError() {
    Error.captureStackTrace(this, arguments.callee);
    this.message = "Requested data not found.";
}
NotFoundError.prototype.toString = function() {
	return "NotFoundError: " + util.inspect(this.message);
};

DataHelper.prototype.DisposedError = DisposedError;
DataHelper.prototype.NotFoundError = NotFoundError;