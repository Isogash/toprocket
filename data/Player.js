module.exports = Player;

function Player(id, wins, draws, losses, position, username) {
    this.id = id;
    this.wins = wins;
    this.draws = draws;
    this.losses = losses;
    this.position = position;
    this.username = username;
}