var async = require('async');
var util = require('util');
var _ = require('lodash');

var logger = require('./logger');
var config = require('./config');

var http = require('http');
var path = require('path');
var socketio = require('socket.io');
var express = require('express');
var wp_auth = !config.wp_auth.mock ?
                require('./auth/wp-auth').create(config.wp_auth.url,
                      config.wp_auth.LOGGED_IN_KEY,
                      config.wp_auth.LOGGED_IN_SALT,
                      config.wp_auth.mysql_host,
                      config.wp_auth.mysql_username,
                      config.wp_auth.mysql_password,
                      config.wp_auth.mysql_database,
                      config.wp_auth.wp_prefix)
              : require("./auth/wp-mock").create();

var dataHelper = new  (require("./data/DataHelper"))(config);
(require("./cleanup.js")).setCleanup(cleanup);

function cleanup() {
    dataHelper.dispose();
}


//
// ## SimpleServer `SimpleServer(obj)`
//
// Creates a new instance of SimpleServer with the following options:
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
//
var router = express();

//Log all requests
router.use(function(req, res, next) {
  logger.debug("Request: " + req.url);
  next();
});

var server = http.createServer(router);
var io = socketio.listen(server);


// If not logged in when going to index.html, redirect to WP login page
if (config.wp_auth.required) {
  router.use('/', authenticateUser);
  router.use('/index.html', authenticateUser);
  io.set("authorization", function(handshake, accept) {
    wp_auth.checkAuth(handshake).on('auth', function (auth_is_valid, user) {
      logger.debug("Auth: %s, user: %j", auth_is_valid, user);
      
      if (auth_is_valid) { // true if the user is logged in
        handshake.wp_user = user;
        accept(null, true);
      }
      else {
        accept(new Error("Could not be authenticated."), false);
      }
    });
  });
}
function authenticateUser(req, res, next) {
  wp_auth.checkAuth(req).on('auth', function (auth_is_valid, user) {
      logger.debug("Auth: %s, user: %j", auth_is_valid, user);
    
    if (auth_is_valid) { // true if the user is logged in
      // Set session variables for top rocket
      //  (NB: we aren't actually using sessions, but this doesn't matter because all the interaction is on one page)
      // ...
        req.wp_user = user;
      
      // Carry on with serving the request
      next();
    }
    else {
      // Redirect to the login page
      try {
        if (config.wp_auth.mock) {
          res.send("nope.");
        } else {
          res.redirect(config.wp_auth.loginUrl);
        }
      } catch (err) {
        logger.error(err);
      }
    }
  });
}

router.use(express.static(path.resolve(__dirname, 'client')));

var sockets = [];
var users = []; // Indexed on id
// ID:int
// user_name:string
// display_name:string
var players = {}; // all player objects set and get with players[username]
// username:string - machine name
// display_name:string - readable name
// queuing:boolean - are they queuing
// game:int - game, undefined is no game, use delete player.game
// hand:array - cards in current hand
// socket:Socket - socket connected on, undefined is not connected

var games = [];

var queue = []; // game queue for players


// Get pack info (NOTICE hardcoded)
var pack = null;
dataHelper.getPacks(function(err, obj) {
  if (err) {
    return logger.error("Error getting pack.", err);
  }
  
  pack = obj[0];
});
// Get cards from database (NOTICE the hardcoded pack_id)
var completeDeck = [];
dataHelper.getCards(1, function (err, obj) {
  if (err) {
    return logger.error("Error getting cards.", err);
  }
  
  completeDeck = obj;
});

// Get users
wp_auth.getUsers(function(results) {
  users = results;
})

io.on('connection', function (socket) {

    sockets.push(socket);
    
    socket.wp_user = socket.handshake.wp_user;
    
    // NB: socket should have wp_user from auth
    if (!!!socket.wp_user){
      logger.error(new Error("Socket connection doesn't have a wp_user property"));
      socket.emit("error", "Authentication error");
      return;
    }
    
    var username = socket.wp_user.user_login;
    if (username in players) {
      // We already have the data for the player - just update the socket
      socket.player = players[username];
      if('socket' in socket.player) {
        socket.player.socket.disconnect();
      }
      socket.player.socket = socket;
      
      playerLoggedIn();
    } else {
      // The username is not present, so the player data hasn't been loaded yet.
      // Load player data
      dataHelper.getPlayer(socket.wp_user.id, function(err, obj) {
        if (!!err && !(err instanceof dataHelper.NotFoundError)) {
          logger.error("", err);
          socket.emit("error", "Internal error");
          return;
        }
        
        if (!!obj) {
          // obj contains the loaded player data
          playerLoggedIn(obj);
        } else {
          dataHelper.addPlayer(socket.wp_user.id, function(err, obj) {
            if (!!err || !!!obj) {
              err = err || new Error("Data: addPlayer returned a falsey player");
              logger.error(err);
              socket.emit("error", "Internal error");
              return;
            }
            
            playerLoggedIn(obj);
          });
        }
      });
    }
    
    function playerLoggedIn(playerDataObj) {
      if (!!playerDataObj) {
        // Set up player obj
        players[username] = new Player(socket.wp_user.id, username, socket, playerDataObj, socket.wp_user.display_name);
        socket.player = players[username];
      } else if (!!!socket.player.playerData) {
        logger.error(new Error("Player data is falsey!"));
        socket.emit("error", "Internal error");
        return;
      }
            
      socket.emit('login', socket.player.getWireVersion());
      broadcast('playerlistadd', {
        player: socket.player.getWireVersion(),
        wins: socket.player.playerData.wins,
        online:true,
        ingame:false
      });
      
      logger.debug("Player list", _.pluck(players, 'username'));
    }

    socket.on('disconnect', function () {
      if('player' in socket) {
        broadcast('playerlistremove', socket.player.username);
        if('game' in socket.player) {
          removePlayerFrom(socket.player.game, socket.player);
        }
        if(socket.player.queuing) {
          queue.splice(queue.indexOf(socket.player), 1);
          socket.player.queuing = false;
          
          if ('queueTimeout' in socket.player) {
            clearTimeout(socket.player.queueTimeout);
            delete socket.player.queueTimeout;
          }
        }
        if('challenge' in socket.player) {
          if(socket.player.challenger) {
            if('socket' in socket.player.challenge) {
              socket.player.challenge.socket.emit('challengecancelled', socket.player.username);
            }
            delete socket.player.challenge.challenge;
            delete socket.player.challenger;
            delete socket.player.challenge;
          } else {
            if('socket' in socket.player.challenge) {
              socket.player.challenge.socket.emit('challengedeclined', socket.player.username);
            }
            delete socket.player.challenge.challenger;
            delete socket.player.challenge.challenge;
            delete socket.player.challenge;
          }
        }
        if('socket' in socket.player && socket.player.socket == socket) {
          delete socket.player.socket;
        }
      }
      sockets.splice(sockets.indexOf(socket), 1);
    });
    
    socket.on('leave', function() {
      if('player' in socket && 'game' in socket.player) {
        removePlayerFrom(socket.player.game, socket.player);
      }
    });
    
    socket.on('getplayerlist', function () {
      var playerlist = [];
      for(var player in players) {
        if('socket' in players[player]) {
          playerlist.push({
            player: players[player].getWireVersion(),
            wins: players[player].playerData.wins,
            online: true,
            ingame: ('game' in players[player])
          });
        }
      }
      socket.emit('playerlist', playerlist);
    });
    
    socket.on('startqueue', function () {
      if('player' in socket && !socket.player.queuing && !('game' in socket.player) && !('challenge' in socket.player)) {
        queue.push(socket.player);
        socket.player.queuing = true;
        socket.emit('joinedqueue');
        
        if ('queueTimeout' in socket.player) {
          clearTimeout(socket.player.queueTimeout);
          delete socket.player.queueTimeout;
        }
        socket.player.queueTimeout = setTimeout(function () { checkForGame(socket.player); }, (Math.floor(Math.random() * 10000)) + 1000);
      }
    });
    
    socket.on('cancelqueue', function () {
      if('player' in socket && socket.player.queuing) {
        queue.splice(queue.indexOf(socket.player), 1);
        socket.player.queuing = false;
        if ('queueTimeout' in socket.player) {
          clearTimeout(socket.player.queueTimeout);
          delete socket.player.queueTimeout;
        }
        socket.emit('leftqueue');
      }
    });
    
    socket.on('playturn', function(stat) {
      if('player' in socket && 'game' in socket.player && socket.player.game.waiting && socket.player == socket.player.game.playerturn)
      {
        if(socket.player.game.waitingForStartTimeout) {
          clearTimeout(socket.player.game.waitingForStartTimeout);
        }
        endTurn(socket.player.game, stat);
      }
    });
    
    socket.on('endturn', function() {
      if('player' in socket && 'game' in socket.player && socket.player.game.waitingForEnd && socket.player == socket.player.game.winnerOfTurn)
      {
        if(socket.player.game.waitingForEndTimeout) {
          clearTimeout(socket.player.game.waitingForEndTimeout);
        }
        newTurn(socket.player.game);
      }
    });
    
    socket.on('challenge', function(target) {
      if('player' in socket && !('game' in socket.player) && !socket.player.queuing) {
        if(players[target] && players[target].username != socket.player.username) {
          if(!('game' in players[target]) && ('socket' in players[target])) {
            if(!players[target].queuing && !('challenge' in players[target])) {
              socket.player.challenge = players[target];
              socket.player.challenger = true;
              players[target].challenge = socket.player;
              socket.emit('challengesent', target);
              players[target].socket.emit('challengerecieved', socket.player.display_name);
            } else {
              socket.emit('challengeblocked', target);
            }
          }
        }
      }
    });
    
    socket.on('challengeaccept', function() {
      if('player' in socket && 'challenge' in socket.player && !socket.player.challenger) {
        var newgamelist = [socket.player, socket.player.challenge];
        var challenger = socket.player;
        delete socket.player.challenge.challenger;
        delete socket.player.challenge.challenge;
        delete socket.player.challenge;
        createGame(newgamelist, true, challenger);
      }
    });
    
    socket.on('challengedecline', function() {
      if('player' in socket && 'challenge' in socket.player && !socket.player.challenger) {
        if('socket' in socket.player.challenge) {
          socket.player.challenge.socket.emit('challengedeclined', socket.player.username);
        }
        delete socket.player.challenge.challenger;
        delete socket.player.challenge.challenge;
        delete socket.player.challenge;
      }
    });
    
    socket.on('cancelchallenge', function() {
      if('player' in socket && 'challenge' in socket.player && socket.player.challenger) {
        if('socket' in socket.player.challenge) {
          socket.player.challenge.socket.emit('challengecancelled', socket.player.username);
        }
        delete socket.player.challenge.challenge;
        delete socket.player.challenger;
        delete socket.player.challenge;
      }
    });
    
    socket.on('leaderboard', function() {
      dataHelper.getLeaderboard(function (err, leaderboard) {
        if (err) {
          logger.error(err);
          socket.emit('error', "Internal error");
          return;
        }
        
        // Make sure users list is up to date
        wp_auth.getUsers(function(results) {
          users = results;
          dataHelper.addUserInfo(leaderboard, users);
          socket.emit('leaderboard', leaderboard);
        });
      })
    });
  });

function checkForGame(firstplayer) {
  if (!!firstplayer.game || !firstplayer.queuing) {
    return;
  }
  
  shuffle(queue);
  var newgamelist = [firstplayer];
  var fullGame = false;
  queue.forEach(function (player) {
    if(!fullGame && player != firstplayer) {
      fullGame = true;
      newgamelist.push(player);
      createGame(newgamelist, false, undefined);
    }
  });
}

function createGame(playerlist, challenge, challenger) {
  var plOkay = true;
  playerlist.forEach(function (player) {
    if (!!player.game) {
      plOkay = false;
      logger.error("GAME ERROR: Tried to create a game with a player that was already in a game.", playerlist);
    }
  });
  if (!plOkay) {
    return;
  }
  logger.debug("Creating game for %s players: %s", playerlist.length, playerlist.join(", "));
  if (playerlist.length != 2) {
    logger.error("Not 2 players in game!", playerlist);
  }
  
  var playerturn = Math.floor(Math.random() * playerlist.length);
  var game = {
    playerlist: playerlist,
    playerturn: playerlist[playerturn],
    turn: 0,
    playerdata: [],
    challenge: challenge,
    challenger: challenger,
    startTime: +new Date()
  };
  game.playerlist.forEach(function(player) {
    player.queuing = false;
    
    // Make SURE there are no timeouts for any of the players
    if ('queueTimeout' in player) {
      clearTimeout(player.queueTimeout);
      delete player.queueTimeout;
    }
    
    queue.splice(queue.indexOf(player), 1);
    game.playerdata.push({ username: player.username, display_name: player.display_name });
    player.game = game;
    broadcast('playerlistadd', {
      player: player.getWireVersion(),
      wins: player.playerData.wins,
      online:true,
      ingame:true
    });
  });
  dealDeck(shuffleDeck(game.playerlist.length), game.playerlist, game.playerturn);
  games.push(game);
  game.playing = true;
  game.playerdata.forEach(function(playerdata) {
    playerdata.cards = players[playerdata.username].hand.length;
  });
  game.playerlist.forEach(function(player) {
    player.socket.emit('newgame', { playerdata: game.playerdata, packdata: pack }); // PACK DATA SENT HERE
  });
  newTurn(game); // IF I HAD A GAME INTRO SCREEN I'D DELAY THIS :D
}

function newTurn(game) {
  for(var player in game.playerlist) {
    if(game.playerlist[player].hand.length == 0) {
      game.playerlist[player].socket.emit('gameover', false);
      removePlayerFrom(game, game.playerlist[player]);
    }
  }
  game.waitingForEnd = false;
  
  game.turn++;
  game.playerdata.forEach(function(playerdata) {
    playerdata.cards = players[playerdata.username].hand.length;
  });
  game.waiting = true;
  game.playerlist.forEach(function(player) {
    player.socket.emit('newturn', { playerturn: game.playerturn.username, turn: game.turn, yourcard: completeDeck[player.hand[0]] });
  });
  game.waitingForStartTimeout = setTimeout(endTurn, 15000, game, Math.floor(Math.random() * pack.stats.length));
}

function endTurn(game, stat) {
  game.waiting = false;
  var winner = game.playerturn;
  for(var player in game.playerlist) {
    if(game.playerlist[player] != winner) {
      if(completeDeck[game.playerlist[player].hand[0]].stats[stat].value > completeDeck[winner.hand[0]].stats[stat].value) {
        winner = game.playerlist[player];
      }
    }
  }
  game.winnerOfTurn = winner;
  var cardsplayed = {};
  for(var player in game.playerlist) {
    var cardNumber = game.playerlist[player].hand.shift();
    cardsplayed[game.playerlist[player].username] = completeDeck[cardNumber];
    winner.hand.push(cardNumber);
  }
  game.playerdata.forEach(function(playerdata) {
    playerdata.cards = players[playerdata.username].hand.length;
  });
  for(var player in game.playerlist) {
    game.playerlist[player].socket.emit('endturn', { winner:winner.username, whoplayer: game.playerturn.username, stat: stat, cardsplayed: cardsplayed, playerdata: game.playerdata });
  }
  if(game.playing) {
    game.playerturn = winner;
    game.waitingForEnd = true;
    game.waitingForEndTimeout = setTimeout(newTurn, 5000, game);
  }
}

function removePlayerFrom(game, player) {
  player.socket.emit('playerleft', player.username);
  broadcast('playerlistadd', { player: player.getWireVersion(), wins: player.playerData.wins, online: true, ingame: false });
  game.playerlist.splice(game.playerlist.indexOf(player), 1);
  delete player.game;
  if(game.playerlist.length <= 1) {
    if(game.playerlist.length == 1) {
      //TODO: Check this, and perhaps neaten it up a bit
      // The other player just won ('player' lost)
      var time = +new Date();
      var p1 = game.playerlist[0].id,
        p2 = player.id;
      if(!game.challenge && config.challengeGamesAreValid) {
        var challenger = !!game.challenge ? game.challenger : null;
        dataHelper.saveGameResult(p1, p2, time, 1, challenger, function(err, saved) {
          if (err) {
            logger.error("RESULT NOT SAVED: %d, %d, %d, %d, %s", p1, p2, time, 1, challenger, err);
          } else {
            logger.debug("RESULT SAVED: %d, %d, %d, %d, %s", p1, p2, time, 1, challenger, err);
          }
        });
        game.playerlist[0].playerData.wins++;
        player.playerData.losses++;
      }
      game.playerlist[0].socket.emit('gameover', true);
      
      removePlayerFrom(game, game.playerlist[0]);
    }
    games.splice(games.indexOf(game), 1);
    game.playing = false;
    game.waiting = false;
  }
}

function shuffleDeck(players) {
  var deck = [];
  for(var i = 0; i < completeDeck.length; i++) {
    deck.push(i);
  };
  return shuffle(deck);
}

function dealDeck(deck, playerlist, firstplayer) {
  // Limit cards in each game
  deck = deck.slice(0, config.numberOfCardsInGame);
  
  playerlist.forEach(function(player) {
    player.hand = [];
  });
  var counter = playerlist.indexOf(firstplayer);
  while(deck.length > 0) {
    playerlist[counter].hand.push(deck.shift());
    counter++;
    if(counter == playerlist.length) {
      counter = 0;
    }
  }
}

function shuffle(array) {
    var counter = array.length, temp, index;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

function broadcast(event, data) {
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}

server.listen(process.env.PORT || config.port || 3000, process.env.IP || config.ip || "0.0.0.0", function(){
  var addr = server.address();
  logger.info("TopRocket server listening at %s:%d", addr.address, addr.port);
});



// TODO: might get intellisense if this is in a separate module?
/* Use a player class
 ************************************************/
function Player(id, username, socket, playerData, display_name) {
 this.id = id;
 this.username = username;
 this.socket = socket;
 this.playerData = playerData;
 this.display_name = display_name;
 
 // Used, but not init
 //this.game = undefined;
 //this.queuing = undefined;
 //this.challenger = undefined;
 //this.challege = undefined;
 //this.hand = undefined;
}
Player.prototype.getWireVersion = function() {
  return {
    id: this.id,
    username: this.username,
    display_name: this.display_name
  };
}